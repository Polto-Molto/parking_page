<?php
session_start();
require("dbconnection.php");
$result  = mysqli_query($conn ,"select * from parking_space");
$list = array();

while($subject_data = mysqli_fetch_array($result))
{
$lot_id = $subject_data['lot_id'];
$block_id = $subject_data['block_id'];
$space_id = $subject_data['space_id'];
if($subject_data['space_status'] == 0)
  $space_status = '<font color="green">Free Space </font>';
else
  $space_status = '<font color="red">Busy Space </font>';

$list[] = array('lot_id' => $lot_id, 'block_id' => $block_id, 'space_id' => $space_id , 'space_status' => $space_status);
}
echo json_encode($list);
?>
